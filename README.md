# zonetap

[![CI Status](http://img.shields.io/travis/Drew Denstedt/zonetap.svg?style=flat)](https://travis-ci.org/Drew Denstedt/zonetap)
[![Version](https://img.shields.io/cocoapods/v/zonetap.svg?style=flat)](http://cocoapods.org/pods/zonetap)
[![License](https://img.shields.io/cocoapods/l/zonetap.svg?style=flat)](http://cocoapods.org/pods/zonetap)
[![Platform](https://img.shields.io/cocoapods/p/zonetap.svg?style=flat)](http://cocoapods.org/pods/zonetap)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

zonetap is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "zonetap"
```

## Author

Drew Denstedt, drew.denstedt@technolica.com

## License

zonetap is available under the MIT license. See the LICENSE file for more info.
