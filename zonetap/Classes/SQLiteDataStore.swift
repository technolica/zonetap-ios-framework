//
//  SQLiteDataStore.swift
//  Pods
//
//  Created by Drew Denstedt on 1/31/17.
//
//

import Foundation
import SQLite


enum DataAccessError: Error {
    case Datastore_Connection_Error
    case Insert_Error
    case Delete_Error
    case Search_Error
    case Nil_In_Data
}


// MARK: - SQLiteDataStore Class

public class SQLiteDataStore {
    
    public static let sharedInstance = SQLiteDataStore()
    let BBDB: Connection?
    
    private init() {
        let dirs: [NSString] =
            NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                                                FileManager.SearchPathDomainMask.allDomainsMask, true) as [NSString]
        let dir = dirs[0]
        let path = dir.appendingPathComponent("zonetap.sqlite")
        
        do {
            BBDB = try Connection(path)
        } catch _ {
            BBDB = nil
        }
    }
    
    func createTables() throws {
        do {
            try CouponDataHelper.createTable()
            try DeviceDataHelper.createTable()
            try RequestPauseDataHelper.createTable()
            
        } catch {
            
            throw DataAccessError.Datastore_Connection_Error
        }
    }
}
