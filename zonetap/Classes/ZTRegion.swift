//
//  ZTRegion.swift
//  ZoneDemo
//
//  Created by nlampi on 2/23/19.
//  Copyright © 2019 ZoneTap. All rights reserved.
//

import UIKit
import CoreLocation

struct ZTPreferencesKeys {
    static let savedItems = "ZTSavedItems"
}

class ZTRegion: NSObject, Codable {
    
    enum EventType: String {
        case onEntry = "On Entry"
        case onExit = "On Exit"
    }
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude, radius, identifier, coupon, eventType
    }
    
    var coordinate: CLLocationCoordinate2D
    var radius: CLLocationDistance
    var identifier: String
    var coupon: ZoneTapCoupon
    var eventType: EventType
    
    init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, identifier: String, coupon: ZoneTapCoupon, eventType: EventType) {
        self.coordinate = coordinate
        self.radius = radius
        self.identifier = identifier
        self.coupon = coupon
        self.eventType = eventType
    }
    
    
    // MARK: - Codable
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let latitude = try values.decode(Double.self, forKey: .latitude)
        let longitude = try values.decode(Double.self, forKey: .longitude)
        coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        radius = try values.decode(Double.self, forKey: .radius)
        identifier = try values.decode(String.self, forKey: .identifier)
        coupon = try values.decode(ZoneTapCoupon.self, forKey: .coupon)
        let event = try values.decode(String.self, forKey: .eventType)
        eventType = EventType(rawValue: event) ?? .onEntry
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinate.latitude, forKey: .latitude)
        try container.encode(coordinate.longitude, forKey: .longitude)
        try container.encode(radius, forKey: .radius)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(coupon, forKey: .coupon)
        try container.encode(eventType.rawValue, forKey: .eventType)
    }
    
}

extension ZTRegion {
    public class func allGeoRegions() -> [ZTRegion] {
        guard let savedData = UserDefaults.standard.data(forKey: ZTPreferencesKeys.savedItems) else { return [] }
        let decoder = JSONDecoder()
        if let savedGeoRegions = try? decoder.decode(Array.self, from: savedData) as [ZTRegion] {
            return savedGeoRegions
        }
        return []
    }
}
