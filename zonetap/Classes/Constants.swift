//
//  Constants.swift
//  Pods
//
//  Created by Drew Denstedt on 1/31/17.
//
//

import Foundation
import UIKit


// MARK: - Constants Class

public class Constants {
    static let serial = UIDevice.current.identifierForVendor
    static let model = UIDevice.current.model
    
    // Earth's gravity
    static let gravity_earth = 9.80665
    
    static let distanceFilter:Double = 2
    
    
    // MARK: - Production Settings
    
    // Base URL
    static let url = "https://app.zonetap.com/#"
    
    // API Endpoints
    static let url_active_coupon = "https://app.zonetap.com/api/coupons/active"
    static let url_active_coupon_radius = "https://app.zonetap.com/api/coupons/active/radius"
    static let url_coupon_served = "https://app.zonetap.com/api/coupons/served"
    static let url_device = "https://app.zonetap.com/api/device"
    
    
    // MARK: - Staging Settings
    
    /*
     // Base URL
     static let url = "https://zonetap-dev.herokuapp.com/#"
     
     // API Endpoints
     static let url_active_coupon = "https://zonetap-dev.herokuapp.com/api/coupons/active"
     static let url_active_coupon_radius = "https://zonetap-dev.herokuapp.com/api/coupons/active/radius"
     static let url_coupon_served = "https://zonetap-dev.herokuapp.com/api/coupons/served"
     static let url_device = "https://zonetap-dev.herokuapp.com/api/device"
     */
}
