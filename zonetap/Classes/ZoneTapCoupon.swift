//
//  ZoneTapCoupon.swift
//  zonetap
//
//  Created by nlampi on 2/25/18.
//

import Foundation
import SwiftyJSON


// MARK: - ZoneTapCoupon Class

class ZoneTapCoupon: NSObject, Codable {
    var couponID:String
    var companyID:String
    let location:JSON
    let zones:JSON
    var title:String
    var details:String
    var coupon:String
    var isNotificationOnly:Bool
    
    enum CodingKeys: String, CodingKey {
        case couponID, companyID, location, zones, title, details, coupon, isNotificationOnly
    }
    
    init(JSONArray:JSON) {
        self.couponID = JSONArray["_id"].stringValue
        self.companyID = JSONArray["company_id"].stringValue
        self.title = JSONArray["title"].stringValue
        self.details = JSONArray["details"].stringValue
        self.coupon = JSONArray.rawString()!
        self.isNotificationOnly = JSONArray["notification_only"].boolValue
        self.location = JSONArray["location"]
        self.zones = JSONArray["zones"]
    }
    
    // MARK: Codable
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        couponID = try values.decode(String.self, forKey: .couponID)
        companyID = try values.decode(String.self, forKey: .companyID)
        location = try values.decode(JSON.self, forKey: .location)
        zones = try values.decode(JSON.self, forKey: .zones)
        title = try values.decode(String.self, forKey: .title)
        details = try values.decode(String.self, forKey: .details)
        coupon = try values.decode(String.self, forKey: .coupon)
        isNotificationOnly = try values.decode(Bool.self, forKey: .isNotificationOnly)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(couponID, forKey: .couponID)
        try container.encode(companyID, forKey: .companyID)
        try container.encode(location, forKey: .location)
        try container.encode(zones, forKey: .zones)
        try container.encode(title, forKey: .title)
        try container.encode(details, forKey: .details)
        try container.encode(coupon, forKey: .coupon)
        try container.encode(isNotificationOnly, forKey: .isNotificationOnly)
    }
}
