//
//  DataModel.swift
//  Pods
//
//  Created by Drew Denstedt on 1/31/17.
//
//

import Foundation

/**
 ZoneTap Data Models
 */

// coupon fields
public typealias Coupon = (
    _id: Int64?,
    coupon_id: String?,
    coupon_served: Int64?,
    coupon: String?
)

// device fields
typealias Device = (
    _id: Int64?,
    device_id: String?,
    api_key: String?,
    firebase_token: String?
)

// request_pause fields
typealias RequestPause = (
    _id: Int64?,
    is_paused: Int64?,
    pause_duration: Int64?
)
