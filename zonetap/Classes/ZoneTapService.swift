//
//  ZoneTapService.swift
//  Pods
//
//  Created by Drew Denstedt on 2/2/17.
//
//

import Foundation
import CoreLocation
import CoreMotion
import SwiftyJSON


// MARK: - ZoneTapLocation Class

/**
 `ZoneTapService` is the primary class for working with ZoneTap. 
 */
public class ZoneTapService {
    
    
    // MARK: - Public Properties
    
    private var _apiKey:String?
    public var apiKey:String? {
        get {
            if _apiKey == nil {
                _apiKey = self.storedApiKey()
            }
            
            return _apiKey
        }
    }
    private var _deviceID:String?
    public var deviceID:String? {
        get {
            if _deviceID == nil {
                _deviceID = self.storedDeviceID()
            }
            
            return _deviceID
        }
    }
    public var isLocationManagerRunning:Bool {
        get {
            
            return self.location.isLocationManagerRunning
        }
    }
    public var isLocationEnabled:Bool {
        get {
            
            return self.location.isLocationEnabled
        }
    }
    
    
    // MARK: - Private Properties
    
    // FIXME: Change names to be more appropriate
    private lazy var location:ZoneTapLocation = {
        
        return ZoneTapLocation(withService: self)
    }()
    
    
    // MARK: - Init
    
    public init() {
        // Create tables if they don't exist already
        createTables()
    }
    
    
    // MARK: - Public Methods
    
    // init method to make sure user gets the location prompt notifications
    public func startLocationListener() {
        self.location.startLocationListener()
    }
    
    // send device firebase token to server
    public func storeFirebaseToken(api_key: String, token: String) {
        self.deviceInsertWithFirebase(api_key: api_key, token: token)
    }
    
    // Handle remote messages and start the location listener if not already started.
    public func processRemoteMessage(appData:[AnyHashable : Any]) {
        if let doDistanceCheck = appData["do_distance_check"] as? String, doDistanceCheck == "true" {
            self.startLocationListener()
        }
    }
    
    
    // MARK: - Private Methods
    
    fileprivate func storedApiKey() -> String? {
        do {
            if let devices = try DeviceDataHelper.findAll() {
                if devices.count > 0 {
                    let device = devices.first
                    
                    return device!.api_key
                }
            }
        } catch _ {
            // Do Nothing?
        }
        
        return nil
    }
    
    fileprivate func createTables() {
        let dataStore = SQLiteDataStore.sharedInstance
        
        do {
            // will create tables if they don't exist
            try dataStore.createTables()
        } catch _ {
            // Do Nothing?
        }
    }
    
    // check device table for active token and device id
    fileprivate func storedDeviceID() -> String? {
        do {
            if let deviceArray = try DeviceDataHelper.findAll() {
                if deviceArray.count > 0 {
                    let device = deviceArray.first
                    
                    return device?.device_id
                }
            }
        } catch _ {
            // Do Nothing?
        }
        
        return nil
    }
    
    // update client table with new device id from server
    fileprivate func deviceInsertWithFirebase(api_key: String, token: String) {
        RestApiManager.sharedInstance.newDeviceWithToken(api_key: api_key, token: token, onCompletion: { (json: JSON) in
            print("ZoneTap: Insert new device")
            let id_device = json["id"].stringValue
            
            if id_device.isEmpty {
                print("ZoneTap: No device update needed")
                return
            }
            
            // Clear existing device
            self.truncateDeviceTable()
            
            do {
                let _id = try DeviceDataHelper.insert(
                    item: Device(
                        _id: 0,
                        device_id: id_device,
                        api_key: api_key,
                        firebase_token: token
                    )
                )
                
                print("ZoneTap: Inserted new device \(_id)")
            } catch _{
                // Do Nothing?
            }
            
            self._deviceID = id_device
        })
    }
    
    // delete all devices data in db
    fileprivate func truncateDeviceTable() {
        do {
            if let devices = try DeviceDataHelper.findAll() {
                print("ZoneTap: Found \(devices.count) devices")
                
                for device in devices {
                    try DeviceDataHelper.delete(item: device)
                    print("ZoneTap: Deleted device \(device.device_id ?? "")")
                }
            }
        } catch _ {
            // Do Nothing?
        }
    }
}
