//
//  DataHelper.swift
//  Pods
//
//  Created by Drew Denstedt on 1/31/17.
//
//

import Foundation
import SQLite


// MARK: - Data Helper Protocol

protocol DataHelperProtocol {
    associatedtype T
    static func createTable() throws -> Void
    static func insert(item: T) throws -> Int64
    static func delete(item: T) throws -> Void
    static func findAll() throws -> [T]?
}


// MARK: - CouponDataHelper Class

public class CouponDataHelper: DataHelperProtocol {
    static let TABLE_NAME = "Coupon"
    
    static let table = Table(TABLE_NAME)
    static let _id = Expression<Int64>("_id")
    static let coupon_id = Expression<String>("coupon_id")
    static let coupon_served = Expression<Int64>("coupon_served")
    static let coupon = Expression<String>("coupon")
    
    // development purposes only this is public as it is used in the sample app table view
    public typealias T = Coupon
    
    static func createTable() throws {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        do {
            let _ = try DB.run( table.create(ifNotExists: true) {t in
                t.column(_id, primaryKey: true)
                t.column(coupon_id, unique: true)
                t.column(coupon_served)
                t.column(coupon)
            })
        } catch _ {
            // Error throw if table already exists
        }
    }
    
    static func insert(item: T) throws -> Int64 {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if (item.coupon_id != nil && item.coupon_served != nil && item.coupon != nil) {
            let insert = table.insert(coupon_id <- item.coupon_id!, coupon_served <- item.coupon_served!, coupon <- item.coupon!)
            
            do {
                let rowId = try DB.run(insert)
                
                guard rowId > 0 else {
                    throw DataAccessError.Insert_Error
                }
                
                return rowId
            } catch _ {
                
                throw DataAccessError.Insert_Error
            }
        }
        
        throw DataAccessError.Nil_In_Data
    }
    
    static func delete (item: T) throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if let id = item._id {
            let query = table.filter(_id == id)
            
            do {
                let tmp = try DB.run(query.delete())
                
                guard tmp == 1 else {
                    
                    throw DataAccessError.Delete_Error
                }
            } catch _ {
                
                throw DataAccessError.Delete_Error
            }
        }
    }
    
    // find all coupons by ids
    static func find(_coupon_id: String) throws -> T? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        let query = table.filter(coupon_id == _coupon_id)
        
        guard let items = try? DB.prepare(query) else {
            
            return nil
        }
        
        for item in  items {
            
            return Coupon(_id: item[_id] , coupon_id: item[coupon_id], coupon_served: item[coupon_served], coupon: item[coupon])
        }
        
        return nil
    }
    
    public static func findAll() throws -> [T]? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        var retArray = [T]()
        
        guard let items = try? DB.prepare(table) else {
            
            return nil
        }
        
        for item in items {
            retArray.append(Coupon(_id: item[_id], coupon_id: item[coupon_id], coupon_served: item[coupon_served], coupon: item[coupon]))
        }
        
        return retArray
    }
}


// MARK: - DeviceDataHelper Class

class DeviceDataHelper: DataHelperProtocol {
    static let TABLE_NAME = "Device"
    
    static let table = Table(TABLE_NAME)
    static let _id = Expression<Int64>("_id")
    static let device_id = Expression<String>("device_id")
    static let api_key = Expression<String>("api_key")
    static let firebase_token = Expression<String>("firebase_token")
    
    typealias T = Device
    
    static func createTable() throws {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        do {
            let _ = try DB.run( table.create(ifNotExists: true) {t in
                t.column(_id, primaryKey: true)
                t.column(device_id, unique: true)
                t.column(api_key, unique: true)
                t.column(firebase_token, unique: true)
            })
        } catch _ {
            // Error throw if table already exists
        }
    }
    
    static func insert(item: T) throws -> Int64 {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if (item.device_id != nil && item.api_key != nil && item.firebase_token != nil) {
            let insert = table.insert(device_id <- item.device_id!, api_key <- item.api_key!, firebase_token <- item.firebase_token!)
            
            do {
                let rowId = try DB.run(insert)
                
                guard rowId > 0 else {
                    throw DataAccessError.Insert_Error
                }
                
                return rowId
            } catch _ {
                
                throw DataAccessError.Insert_Error
            }
        }
        
        throw DataAccessError.Nil_In_Data
    }
    
    static func delete(item: T) throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if let id = item._id {
            let query = table.filter(_id == id)
            
            do {
                let tmp = try DB.run(query.delete())
                
                guard tmp == 1 else {
                    
                    throw DataAccessError.Delete_Error
                }
            } catch _ {
                
                throw DataAccessError.Delete_Error
            }
        }
    }
    
    static func findAll() throws -> [T]? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        var retArray = [T]()
        
        guard let items = try? DB.prepare(table) else {
            
            return nil
        }
        
        for item in items {
            retArray.append(Device(_id: item[_id], device_id: item[device_id], api_key: item[api_key], firebase_token: item[firebase_token]))
        }
        
        return retArray
    }
}


// MARK: - RequestPauseDataHelper Class

public class RequestPauseDataHelper: DataHelperProtocol {
    static let TABLE_NAME = "RequestPause"
    
    static let table = Table(TABLE_NAME)
    static let _id = Expression<Int64>("_id")
    static let is_paused = Expression<Int64>("is_paused")
    static let pause_duration = Expression<Int64>("pause_duration")
    
    typealias T = RequestPause
    
    static func createTable() throws {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        do {
            let _ = try DB.run( table.create(ifNotExists: true) {t in
                t.column(_id, primaryKey: true)
                t.column(is_paused)
                t.column(pause_duration)
            })
        } catch _ {
            // Error throw if table already exists
        }
    }
    
    static func insert(item: T) throws -> Int64 {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if (item.is_paused != nil && item.pause_duration != nil) {
            let insert = table.insert(is_paused <- item.is_paused!, pause_duration <- item.pause_duration!)
            
            do {
                let rowId = try DB.run(insert)
                
                guard rowId > 0 else {
                    
                    throw DataAccessError.Insert_Error
                }
                
                return rowId
            } catch _ {
                
                throw DataAccessError.Insert_Error
            }
        }
        
        throw DataAccessError.Nil_In_Data
    }
    
    static func delete (item: T) throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if let id = item._id {
            let query = table.filter(_id == id)
            
            do {
                let tmp = try DB.run(query.delete())
                
                guard tmp == 1 else {
                    
                    throw DataAccessError.Delete_Error
                }
            } catch _ {
                
                throw DataAccessError.Delete_Error
            }
        }
    }
    
    static func update (item: T) throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        if let id = item._id {
            let query = table.filter(_id == id)
            
            do {
                try DB.run(query.update([is_paused <- item.is_paused!, pause_duration <- item.pause_duration!]))
            } catch _ {
                
                throw DataAccessError.Nil_In_Data
            }
        }
    }
    
    static func findAll() throws -> [T]? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        var retArray = [T]()
        
        guard let items = try? DB.prepare(table) else {
            
            return nil
        }
        
        for item in items {
            retArray.append(RequestPause(_id: item[_id], is_paused: item[is_paused], pause_duration: item[pause_duration]))
        }
        
        return retArray
    }
}
