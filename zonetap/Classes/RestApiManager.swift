//
//  RestApiManager.swift
//  Pods
//
//  Created by Drew Denstedt on 1/31/17.
//
//

import Foundation
import SwiftyJSON

typealias ServiceResponse = (JSON, NSError?) -> Void


// MARK: - RestApiManager Class

class RestApiManager: NSObject {
    static let sharedInstance = RestApiManager()
    
    // get active coupons of company that client is within geozone
    func getActiveCoupons(api_key: String, lat: String, lon: String, id_device: String, accuracy: String, onCompletion: @escaping (JSON) -> Void) -> URLSessionDataTask {
        let route = Constants.url_active_coupon + "?id_device=" + id_device + "&company=" + api_key + "&lat=" + lat + "&lon=" + lon + "&accuracy=" + accuracy
        
        return makeHTTPGetRequest(path: route, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    // get active coupons for company within a predetermined radius
    func getActiveCouponsWithinRadius(api_key: String, lat: String, lon: String, id_device: String, is_background: String, onCompletion: @escaping (JSON) -> Void) -> URLSessionDataTask {
        let route = Constants.url_active_coupon_radius + "?id_device=" + id_device + "&company=" + api_key + "&lat=" + lat + "&lon=" + lon + "&radius=1&is_background=" + is_background
        
        return makeHTTPGetRequest(path: route, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    // post coupon served with gps accuracy and location of hit
    func postCouponServed(body: String, onCompletion: @escaping (JSON) -> Void) {
        let route = Constants.url_coupon_served
        
        makeHTTPPostRequest(path: route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    // put new device delay and wake time
    func putDeviceWakeTime(body: String, device_id: String, onCompletion: @escaping (JSON) -> Void) {
        let route = Constants.url_device + "/" + device_id
        
        makeHTTPPostRequest(path: route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    // do new device insert with firebase token and save new device id locally
    func newDeviceWithToken(api_key: String, token:String, onCompletion: @escaping (JSON) -> Void) {
        let route = Constants.url_device
        
        let body = "firebase_token=\(token)&id_device=\(Constants.serial!)&id_company=\(api_key)&manufacturer=apple&model=\(Constants.model)"
        
        makeHTTPPostRequest(path: route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    // MARK: Perform a GET Request
    func makeHTTPGetRequest(path: String, onCompletion: @escaping ServiceResponse) -> URLSessionDataTask {
        print("ZoneTap: Making request \(path)")
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            do {
                if let jsonData = data {
                    let json:JSON = try JSON(data: jsonData)
                    onCompletion(json, error as NSError?)
                } else {
                    onCompletion(JSON.null, error as NSError?)
                }
            } catch {
                // Create your personal error
                onCompletion(JSON.null, nil)
            }
        })
        
        task.resume()
        
        return task
    }
    
    // MARK: Perform a POST Request
    func makeHTTPPostRequest(path: String, body: String, onCompletion: @escaping ServiceResponse) {
        print("ZoneTap: Making request \(path)?\(body)")
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        // Set the method to POST
        request.httpMethod = "POST"
        
        // Set the POST body for the request
        request.httpBody = body.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            do {
                if let jsonData = data {
                    let json:JSON = try JSON(data: jsonData)
                    onCompletion(json, nil)
                } else {
                    onCompletion(JSON.null, error as NSError?)
                }
            } catch {
                // Create your personal error
                onCompletion(JSON.null, nil)
            }
        })
        task.resume()
    }
    
    // MARK: Perform a PUT Request
    func makeHTTPPutRequest(path: String, body: String, onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        // Set the method to PUT
        request.httpMethod = "PUT"
        
        // Set the POST body for the request
        request.httpBody = body.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            do {
                if let jsonData = data {
                    let json:JSON = try JSON(data: jsonData)
                    onCompletion(json, nil)
                } else {
                    onCompletion(JSON.null, error as NSError?)
                }
                
            } catch {
                // Create your personal error
                onCompletion(JSON.null, nil)
            }
        })
        task.resume()
    }
}
