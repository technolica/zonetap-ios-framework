//
//  ZoneTapLocation.swift
//  Pods
//
//  Created by Drew Denstedt on 2/2/17.
//
//

import CoreLocation
import MapKit
import CoreData
import UIKit
import SwiftyJSON

// MARK: - Enum

enum ZoneTapLocationManagerErrors: Int {
    case AuthorizationDenied
    case AuthorizationNotDetermined
    case InvalidLocation
}


// MARK: - ZoneTapLocation Class

public class ZoneTapLocation : NSObject, CLLocationManagerDelegate {
    
    
    // MARK: - Private Properties
    
    fileprivate weak var ztService: ZoneTapService?
    fileprivate lazy var locationManager: CLLocationManager = {
        let manager = ZoneTapLocation.createLocationManager()
        manager.delegate = self
        
        return manager
    }()
    private(set) var isLocationEnabled = false
    private(set) var isLocationManagerRunning = false
    fileprivate var isLocalScanning = false
    fileprivate let dataStore = SQLiteDataStore.sharedInstance
    fileprivate var couponDataTask:URLSessionDataTask?
    fileprivate var radiusDataTask:URLSessionDataTask?
    fileprivate var geoRegions: [ZTRegion] = []
    
    
    // MARK: - Init
    
    init(withService service:ZoneTapService) {
        super.init()
        
        self.ztService = service
        
        geoRegions.removeAll()
        let allGeoRegions = ZTRegion.allGeoRegions()
        allGeoRegions.forEach { geoRegions.append($0) }
    }
    
    
    // MARK: - Public Methods
    
    // TODO: Fix this creation logic to make it easier on app to register for location updates
    public static func createLocationManager() -> CLLocationManager {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = Constants.distanceFilter
        manager.requestAlwaysAuthorization()
        manager.allowsBackgroundLocationUpdates = true
        manager.pausesLocationUpdatesAutomatically = true
        manager.activityType = .fitness
        
        return manager
    }
    
    public func startLocationListener() {
        print("ZoneTap: Start location listener")
        
        guard self.isLocationEnabled == false else {
            print("ZoneTap: Error - Location services are not enabled")
            
            return
        }
        
        self.isLocationManagerRunning = true
        
        if self.isLocalScanning {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    
    public func stopLocationListener() {
        print("ZoneTap: Stop location manager")
        self.isLocationManagerRunning = false
        
        if self.isLocalScanning {
            self.locationManager.stopUpdatingLocation()
        } else {
            self.locationManager.stopMonitoringSignificantLocationChanges()
        }
    }
    
    fileprivate func handleRegionEvent(with region: CLRegion!) {
//        let type:ZTRegion.EventType = .onEntry
        
        guard let region = existingRegion(from: region.identifier) else {
            print("ZoneTap: Error - Unable to load region coupon")
            return
        }
        
        guard let deviceID = self.ztService?.deviceID else {
            print("ZoneTap: No Device ID found")
            
            return
        }
        
        do {
            let _coupon = try CouponDataHelper.find(_coupon_id: region.coupon.couponID)
            
            if _coupon == nil {
                let coordinates:JSON = region.coupon.zones[0]["coords"]["geometry"]["coordinates"][0][0]
                
                guard let coords = coordinates.array, coords.count == 2 else {
                    print("ZoneTap: Error - Invalid coordinates")
                    return
                }
                    
                let _lon = coords[0].double
                let _lat = coords[1].double
                let location = CLLocation(latitude: _lat!, longitude: _lon!)
                
                do {
                    _ = try CouponDataHelper.insert(
                        item: Coupon(
                            _id: 0,
                            coupon_id: region.coupon.couponID,
                            coupon_served: 1,
                            coupon: region.coupon.coupon)
                    )
                } catch _{
                    // Do Nothing?
                }
                
                // Remove Zone
                self.removeRegion(region)
                
                // Serve the local notification as long as it's a report only notification
                if !region.coupon.isNotificationOnly {
                    let url = "\(Constants.url)/coupon/view/\(region.coupon.couponID)/\(region.coupon.companyID)/\(deviceID)"
                    
                    self.serveLocalNotification(url: url, title: region.coupon.title, details: region.coupon.details)
                }
                
                self.postCouponServed(location: location, id_coupon: region.coupon.couponID)
            } else {
                print("ZoneTap: found coupon no insert")
            }
        } catch _{
            // Do Nothing?
        }
    }
    
    fileprivate func existingRegion(from identifier: String) -> ZTRegion? {
        guard let matched = self.geoRegions.filter({
            $0.identifier == identifier
        }).first else { return nil }
        return matched
    }
    
    fileprivate func toggleLocalScanning(enabled:Bool) {
        guard enabled != self.isLocalScanning else {
            // Don't toggle when already on the same method
            return
        }
        
        self.stopLocationListener()
        
        self.isLocalScanning = enabled
    }
    
    public func lastKnownLocation() -> CLLocation {
        
        return locationManager.location!
    }
    
    
    // MARK: - Private Methods
    
    fileprivate func analyzeCouponsWith(location: CLLocation?, error: NSError?) {
        if(error != nil) {
            print("ZoneTap: Error in didComplete")
            print("\(error!)")
            
            if let error = error as? CLError, error.code == .denied {
                // Location updates are not authorized.
                self.stopLocationListener()
                return
            }
        }
        
        guard location != nil else {
            print("ZoneTap: No location found in Location didComplete")
            
            return
        }
        
        guard let deviceID = self.ztService?.deviceID else {
            print("ZoneTap: No Device ID found")
            
            return
        }
        
        self.getCouponsWithinRadiusFor(location: location!, with: deviceID) { (coupons) in
            // Remove Regions
            self.geoRegions.forEach { self.removeRegion($0) }
            
            // Add regions
            if coupons.count > 0 {
                for coupon in coupons {
                    let coordinates:JSON = coupon.zones[0]["coords"]["geometry"]["coordinates"][0]
                    
                    if coordinates.count > 1 {
                        do {
                            let _coupon = try CouponDataHelper.find(_coupon_id: coupon.couponID)
                            
                            // coupon hasn't been served, add to list of zones
                            if _coupon == nil, let region = self.coordinateRegion(forCoordinates: coordinates) {
                                let farPoint = CLLocation(latitude: region.center.latitude + (region.span.latitudeDelta / 2),
                                                          longitude: region.center.longitude + (region.span.longitudeDelta / 2))
                                let centerPoint = CLLocation(latitude: region.center.latitude, longitude: region.center.longitude)
                                let radius = centerPoint.distance(from: farPoint)
                                
                                // Add Zone
                                let identifier = NSUUID().uuidString
                                let eventType: ZTRegion.EventType = .onEntry
                                self.addCoordinate(region.center, radius: radius, identifier: identifier, coupon: coupon, eventType: eventType)
                            }
                        } catch {
                            // Do Nothing?
                        }
                        
                    }
                }
            }
        }
    }
    
    fileprivate func coordinateRegion(forCoordinates coordinates:SwiftyJSON.JSON) -> MKCoordinateRegion? {
        var mapCoordinates = [CLLocationCoordinate2D]()
        
        guard let coordinatesArray = coordinates.array else {
            print("ZoneTap: Unable to parse coupon region")
            
            return nil
        }
        
        for coordinatePair in coordinatesArray {
            if let coords = coordinatePair.array, coords.count == 2 {
                let _lon = coords[0].double
                let _lat = coords[1].double
                
                mapCoordinates.append(CLLocationCoordinate2D(latitude: _lat!, longitude: _lon!))
            }
        }
        
        var minLat: CLLocationDegrees = 90.0
        var maxLat: CLLocationDegrees = -90.0
        var minLon: CLLocationDegrees = 180.0
        var maxLon: CLLocationDegrees = -180.0
        
        for coordinate in mapCoordinates {
            minLat = min(minLat, coordinate.latitude)
            minLon = min(minLon, coordinate.longitude)
            
            maxLat = max(maxLat, coordinate.latitude)
            maxLon = max(maxLon, coordinate.longitude)
        }
        
        let coordinateOrigin = CLLocationCoordinate2D(latitude: minLat, longitude:minLon)
        let coordinateMax = CLLocationCoordinate2D(latitude: maxLat, longitude:maxLon)
        
        let upperLeft = MKMapPoint(coordinateOrigin)
        let lowerRight = MKMapPoint(coordinateMax)
        
        let mapRect = MKMapRect(x: upperLeft.x, y: upperLeft.y, width: lowerRight.x - upperLeft.x, height: lowerRight.y - upperLeft.y)
        let region = MKCoordinateRegion(mapRect)
        
        return region
    }
    
    fileprivate func scan(coupons:[ZoneTapCoupon], for location:CLLocation, with deviceID:String) {
        if coupons.count > 0 {
            self.toggleLocalScanning(enabled: true)
            
            self.process(coupons: coupons, for: location, with: deviceID)
            
            self.startLocationListener()
        } else {
            // No longer have coupons in zone, trigger remote wake
            
            self.analyzeCouponRadius(location: location, error: nil)
        }
    }
    
    fileprivate func process(coupons:[ZoneTapCoupon], for location:CLLocation, with deviceID:String) {
        guard coupons.count > 0 else {
            self.toggleLocalScanning(enabled: false)
            
            // Trigger Radius Call
            self.analyzeCouponRadius(location: location, error: nil)
            
            self.startLocationListener()
            return
        }
        
        for coupon in coupons {
            do {
                let _coupon = try CouponDataHelper.find(_coupon_id: coupon.couponID)
                
                if _coupon == nil {
                    do {
                        _ = try CouponDataHelper.insert(
                            item: Coupon(
                                _id: 0,
                                coupon_id: coupon.couponID,
                                coupon_served: 1,
                                coupon: coupon.coupon)
                        )
                    } catch _{
                        // Do Nothing?
                    }
                    
                    // Serve the local notification as long as it's a report only notification
                    if !coupon.isNotificationOnly {
                        let url = "\(Constants.url)/coupon/view/\(coupon.couponID)/\(coupon.companyID)/\(deviceID)"
                        
                        self.serveLocalNotification(url: url, title: coupon.title, details: coupon.details)
                    }
                    
                    self.postCouponServed(location: location, id_coupon: coupon.couponID)
                } else {
                    print("ZoneTap: found coupon no insert")
                }
            } catch _ {
                // Do Nothing?
            }
        }
    }
    
    
    fileprivate func analyzeCouponRadius(location: CLLocation, error: NSError?) {
        if(error != nil) {
            print("ZoneTap: Error in didComplete")
            print("\(error!)")
        }
        
        guard let deviceID = self.ztService?.deviceID else {
            print("ZoneTap: No Device ID found")
            
            return
        }
        
        self.getCouponsWithinRadiusFor(location: location, with: deviceID) { (coupons) in
            if coupons.count > 0 {
                // start with large distance (10,000) miles
//                var shortestDistance:Double = 16093400
                
                // start with large distance (800) miles
                var shortestDistance:Double = 1287472
                
                for coupon in coupons {
                    let coordinates:JSON = coupon.location["coordinates"]
                    
                    if let coords = coordinates.array {
                        if coords.count == 2 {
                            let _lon = coords[0].double
                            let _lat = coords[1].double
                            let zone = CLLocation(latitude: _lat!, longitude: _lon!)
                            
                            // distance in meters
                            let distance = location.distance(from: zone)
                            
                            // update distance as closer zones are calculated
                            if distance < shortestDistance {
                                // zone must not have already been served
                                do {
                                    let _coupon = try CouponDataHelper.find(_coupon_id: coupon.couponID)
                                    
                                    // coupon hasn't been served update distance
                                    if _coupon == nil {
                                        shortestDistance = distance
                                    }
                                } catch _ {
                                    // Do Nothing?
                                }
                            }
                        }
                    }
                }
                
                // convert to miles
                let minute:Double = 60000 // 1 minute in milliseconds
                let closestZoneInMiles = shortestDistance / 1609.344
                print("ZoneTap: closestZoneInMiles \(closestZoneInMiles)")
                
                // determine millisecond delays for zone with distance exceeding .25 miles
                if closestZoneInMiles > 0.25 {
                    // delay on mile a minute calculation
                    let millisecondDelay = minute * closestZoneInMiles // act as default delay for any instance with zone within .25 miles
                    
                    // exceeding .25 miles, update server with next wake
                    let seconds = Int(millisecondDelay / 1000)
                    let currentDate = NSDate()
                    let calendar = NSCalendar.current
                    let date = calendar.date(byAdding: .second, value: seconds, to: currentDate as Date)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                    let dateString = dateFormatter.string(from: date!)
                    
                    let body = "wake_time=\(dateString)&is_sleeping=1"
                    
                    RestApiManager.sharedInstance.putDeviceWakeTime(body: body, device_id: deviceID, onCompletion: { (json: JSON) in
                        print("ZoneTap: PUT device wakup updated")
                        
//                        print(json)
                    })
                } else if shortestDistance < 150 { // TODO: Magic number in meters
                    // If there is a zone within a short radius, trigger back to local updates
                    self.toggleLocalScanning(enabled: true)
                    
                    self.startLocationListener()
                }
            }
        }
    }
    
    
    fileprivate func getActiveCouponsWithinRadius() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                self.startLocationListener()
            case .authorizedAlways, .authorizedWhenInUse:
                self.startLocationListener()
            @unknown default:
                self.startLocationListener()
            }
        } else {
            self.startLocationListener()
        }
    }
    
    fileprivate func getCouponsFor(location:CLLocation, with deviceID:String, onCompletion: @escaping ([ZoneTapCoupon]) -> Void) {
        
        guard let apiKey = self.ztService?.apiKey else {
            print("ZoneTap: No API Key found")
            
            return
        }
        
        guard self.couponDataTask == nil else {
            // Don't trigger if there is already a task.
            
            return
        }
        
        let lat:String = NSNumber(value: location.coordinate.latitude as Double).stringValue
        let lon:String = NSNumber(value: location.coordinate.longitude as Double).stringValue
        let accuracy:String = NSNumber(value: location.horizontalAccuracy as Double).stringValue
        
        self.couponDataTask = RestApiManager.sharedInstance.getActiveCoupons(api_key: apiKey, lat: lat, lon: lon, id_device: deviceID, accuracy: accuracy, onCompletion: { (json: JSON) in
            var returnValue = [ZoneTapCoupon]()
            
            if let results = json.array {
                
                for obj in results {
                    let coupon = ZoneTapCoupon(JSONArray:obj)
                    
                    returnValue.append(coupon)
                }
            }
            
            self.couponDataTask = nil // Reset task status
            
            onCompletion(returnValue)
        })
    }
    
    fileprivate func getCouponsWithinRadiusFor(location:CLLocation, with deviceID:String, onCompletion: @escaping ([ZoneTapCoupon]) -> Void) {
        
        guard let apiKey = self.ztService?.apiKey else {
            print("ZoneTap: No API Key found")
            
            return
        }
        
        guard self.radiusDataTask == nil else {
            // Don't trigger if there is already a task.
            
            return
        }
        
        let lat:String = NSNumber(value: location.coordinate.latitude as Double).stringValue
        let lon:String = NSNumber(value: location.coordinate.longitude as Double).stringValue
        let isBackground = "false" // TODO: Update this logic
        
        self.radiusDataTask = RestApiManager.sharedInstance.getActiveCouponsWithinRadius(api_key: apiKey, lat: lat, lon: lon, id_device: deviceID, is_background: isBackground, onCompletion: { (json: JSON) in
            var returnValue = [ZoneTapCoupon]()
            
            if let results = json.array {
                for obj in results {
                    let coupon = ZoneTapCoupon(JSONArray:obj)
                    
                    returnValue.append(coupon)
                }
            }
            
            self.radiusDataTask = nil
            
            onCompletion(returnValue)
        })
    }
    
    
    // MARK: - API/Network Methods
    // TODO: Move these to a more logical place.
    
    fileprivate func postCouponServed(location: CLLocation, id_coupon: String) {
        guard let deviceID = self.ztService?.deviceID else {
            print("ZoneTap: No Device ID found")
            
            return
        }
        guard let apiKey = self.ztService?.apiKey else {
            print("ZoneTap: No API Key found")
            
            return
        }
        
        let accuracy = NSNumber(value: (location.horizontalAccuracy) as Double)
        let latitude = NSNumber(value: (location.coordinate.latitude) as Double)
        let longitude = NSNumber(value: (location.coordinate.longitude) as Double)
        
        let body = "id_device=\(deviceID)&id_company=\(apiKey)&id_coupon=\(id_coupon)&gps_accuracy=\(accuracy)&latitude=\(latitude)&longitude=\(longitude)"
        
        RestApiManager.sharedInstance.postCouponServed(body: body, onCompletion: { (json: JSON) in
            print("ZoneTap: Coupon posted as served.")
        })
    }
    
    fileprivate func serveLocalNotification(url: String, title: String, details: String) {
        let localNotification = UILocalNotification()
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0) as Date /// Serve immediately
        localNotification.alertTitle = title
        localNotification.alertBody = details
        localNotification.timeZone = NSTimeZone.default
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.userInfo = ["zturl": url]
        //localNotification.category = "zonetap_url"
        
        DispatchQueue.main.async {
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
    }
    
    
    // MARK: - CLLocationManagerDelegate Methods
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("ZoneTap: didUpdateLocation")
        let location = locations.last
        
        analyzeCouponsWith(location: location, error: nil)
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("ZoneTap: didFailWithError")
        
        analyzeCouponsWith(location: nil, error: error as NSError?)
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        if region is CLCircularRegion {
            self.handleRegionEvent(with: region)
        }
    }
    
    @nonobjc public func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
            
        case .authorizedWhenInUse:
            print("ZoneTap: location authorized when in use")
            self.isLocationEnabled = true
            self.startLocationListener()
            
        case .authorizedAlways:
            print("ZoneTap: location authorized always")
            self.isLocationEnabled = true
            self.startLocationListener()
            
        case .notDetermined:
            print("ZoneTap: location not determined")
            self.isLocationEnabled = false
            
        case .restricted:
            print("ZoneTap: location restricted")
            self.isLocationEnabled = false
            
        case .denied:
            print("ZoneTap: location denied")
            analyzeCouponsWith(location: nil, error: NSError(domain: self.classForCoder.description(),
                                                             code: ZoneTapLocationManagerErrors.AuthorizationDenied.rawValue,
                                                             userInfo: nil))
            self.isLocationEnabled = false
        @unknown default:
            print("ZoneTap: Warning - Unkown location status")
            self.isLocationEnabled = false
        }
    }
    
    
    // MARK: - Region Code
    
    
    func startMonitoring(geoRegion: ZTRegion) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            print("ZoneTap: Geofencing is not supported on this device!")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let message = """
      Your geoRegion is saved but will only be activated once you grant
      ZoneTap permission to access the device location.
      """
            print(message)
        }
        
        let fenceRegion = region(with: geoRegion)
        
        locationManager.startMonitoring(for: fenceRegion)
    }
    
    func stopMonitoring(geoRegion: ZTRegion) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geoRegion.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func addCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, coupon: ZoneTapCoupon, eventType: ZTRegion.EventType) {
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geoRegion = ZTRegion(coordinate: coordinate, radius: clampedRadius, identifier: identifier, coupon: coupon, eventType: eventType)
        geoRegions.append(geoRegion)
        startMonitoring(geoRegion: geoRegion)
        saveAllGeoRegions()
    }
    
    func removeRegion(_ geoRegion: ZTRegion) {
        
        guard let index = geoRegions.firstIndex(of: geoRegion) else {
            print("ZoneTap: Warning - unable to find region for removal | \(geoRegion)")
            return
        }
        
        stopMonitoring(geoRegion: geoRegion)
        geoRegions.remove(at: index)
        saveAllGeoRegions()
    }
    
    func saveAllGeoRegions() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(geoRegions)
            UserDefaults.standard.set(data, forKey: ZTPreferencesKeys.savedItems)
        } catch {
            print("ZoneTap: Error encoding geoRegion")
        }
    }
    
    func region(with geoRegion: ZTRegion) -> CLCircularRegion {
        let region = CLCircularRegion(center: geoRegion.coordinate, radius: geoRegion.radius, identifier: geoRegion.identifier)
        region.notifyOnEntry = true //(geoRegion.eventType == .onEntry)
        region.notifyOnExit = false //!region.notifyOnEntry
        
        return region
    }
}
