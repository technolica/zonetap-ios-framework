//
//  AppDelegate.swift
//  zonetap
//
//  Created by Drew Denstedt on 01/31/2017.
//  Copyright (c) 2017 Drew Denstedt. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import zonetap

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    weak var app:UIApplication?
    var window: UIWindow?
    
    let gcmMessageIDKey = "gcm.message_id"
    var ztservice: ZoneTapService?
    var backgroundTask : UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    weak var rootNavigationController:UINavigationController?
    weak var rootViewController:ViewController?
    weak var setupViewController:SetupViewController?
    var tempNotification:UNNotification? // TODO: Figure out a better way to handle the new
    
    
    // MARK: - App Lifecycle Methods
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.app = application
        
        guard let rootNav = self.window?.rootViewController as? UINavigationController, let sVC = rootNav.topViewController as? SetupViewController else {
            print("Invalid Root Controllers")
            
            return false
        }
        UNUserNotificationCenter.current().delegate = self
        
        self.rootNavigationController = rootNav
        self.setupViewController = sVC
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        // Start the ZoneTapService /// TODO: Convert to something more elegant?
        self.ztservice = ZoneTapService()
        
        // App is ready to start /// FIXME: This check is not good enough to verify app status.
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote intanceID: \(error)")
            } else if result != nil {
                self.setupViewController?.checkAppStatus()
            }
        }
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        connectToFcm()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        print("Disconnected from FCM.")
    }
    
    
    // MARK: - Background Task Methods
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("*** Background task trigger ***")
        
        self.ztservice?.startLocationListener()
    }
    
    
    // MARK: - Private Methods
    
    func registerAppForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert, .badge, .sound],
            completionHandler: {_, _ in })
        
        self.app?.registerForRemoteNotifications()
    }
    
    func startZoneTapApplication() {
        if self.setupViewController != nil {
            self.setupViewController?.removeFromParent()
        }
        
        self.rootViewController = self.rootNavigationController?.storyboard?.instantiateViewController(withIdentifier: "mainViewController") as? ViewController
        
        self.rootNavigationController?.setViewControllers([self.rootViewController!], animated: false)
        
        if self.tempNotification != nil {
            self.rootViewController?.open(notification: self.tempNotification!)
            self.tempNotification = nil
        }
        
        // Set minimum background fetch interval for BG checks
        UIApplication.shared.setMinimumBackgroundFetchInterval(30)
        
        self.ztservice?.startLocationListener()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote intanceID: \(error)")
                return
            } else if result != nil {
                // Do nothing
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func endTask() {
        //do all your clean up here, this is called few seconds before the task expires.
        UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTask.rawValue))
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    
    // MARK: - Remote Notification Methods
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
        
        // Handle Error
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // Do Nothing?
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("*** Will Present Notification ***")
        
        let userInfo = notification.request.content.userInfo
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        self.rootViewController?.received(notification: notification)
        
        // Change this to your preferred presentation option
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Open the notification if the user tapped it.
        if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
            if self.rootViewController != nil {
                self.rootViewController?.open(notification: response.notification)
            } else {
                self.tempNotification = response.notification
            }
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("*** Received Remote Notification With Completion Handler ***")
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        print(userInfo)
        
        if let service = self.ztservice {
            service.startLocationListener()
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
        
        if (backgroundTask == UIBackgroundTaskIdentifier.invalid) {
            backgroundTask = UIApplication.shared.beginBackgroundTask { () -> Void in
                self.endTask()
            }
        }
    }
}


// MARK: - Messaging Delegate

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote intanceID: \(error)")
            } else if let result = result {
                let refreshedToken = result.token
                
                self.ztservice!.storeFirebaseToken(api_key: "blueberry", token: refreshedToken)
            }
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("--- Received remote message ---")
        
        self.ztservice?.processRemoteMessage(appData: remoteMessage.appData)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
	return UIBackgroundTaskIdentifier(rawValue: input)
}
