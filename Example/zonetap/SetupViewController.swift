//
//  SetupViewController.swift
//  zonetap_Example
//
//  Created by nlampi on 2/9/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import CoreLocation
import zonetap


// MARK: - SetupViewController Class

class SetupViewController : UIViewController, CLLocationManagerDelegate {
    var appDelegate:AppDelegate!
    
    @IBOutlet weak var introTextLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var locationSettingsButton: UIButton!
    
    var isFirstRun:Bool = true
    var isRegistered:Bool = false
    var isLocationEnabled:Bool = false
    var locationManager:CLLocationManager?
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        if !self.isRegistered {
            self.introTextLabel.isHidden = false
            self.continueButton.isHidden = false
        }
    }
    
    
    // MARK: - Public Methods
    
    func checkAppStatus() {
        self.checkLocationServices()
        self.checkStatus()
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.isLocationEnabled = false
            case .authorizedAlways, .authorizedWhenInUse:
                self.isLocationEnabled = true
            @unknown default:
                print("Warning: Unkown location status")
                self.isLocationEnabled = false
            }
        } else {
            self.isLocationEnabled = false
        }
    }
    
    
    // MARK: - Private Methods
    
    func proceedToMainApp() {
        self.locationManager = nil
        
        self.appDelegate.startZoneTapApplication()
    }
    
    func checkStatus() {
        
        if self.isLocationEnabled {
            DispatchQueue.main.async {
                self.locationSettingsButton.isHidden = true
            }
        } else if !self.isFirstRun {
            DispatchQueue.main.async {
                self.introTextLabel.isHidden = false
                self.locationSettingsButton.isHidden = false
            }
        }
        
        if self.isLocationEnabled {
            self.isRegistered = true
            
            DispatchQueue.main.async {
                self.introTextLabel.text = "You're all set!"
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                self.proceedToMainApp()
            })
        } else if self.isFirstRun {
            self.isFirstRun = false
        }
    }
    
    
    // MARK: - CLLocationManagerDelegate methods
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.checkLocationServices()
        self.checkStatus()
    }
    
    
    
    // MARK: - Gesture Recognizers
    
    @IBAction func didTapLocationButton(_ sender: Any) {
        self.launchSettings()
    }
    
    func launchSettings() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
            if UIApplication.shared.canOpenURL(appSettings) {
                UIApplication.shared.open(appSettings)
            }
        }
    }
    
    @IBAction func didTapContinue(_ sender: Any) {
        if !self.isRegistered {
            self.continueButton.isHidden = true
            self.locationManager = ZoneTapLocation.createLocationManager()
            self.locationManager?.delegate = self
            self.appDelegate.registerAppForNotifications()
        } else {
            // Continue to main app.
            
            self.proceedToMainApp()
        }
    }
}
