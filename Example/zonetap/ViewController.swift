//
//  ViewController.swift
//  zonetap
//
//  Created by Drew Denstedt on 01/31/2017.
//  Copyright (c) 2017 Drew Denstedt. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var notificationTableView: UITableView!
    var notifications = [UNNotification]()
    var selectedZtUrl = ""
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? CouponDetailsViewController {
            destVC.ztUrl = self.selectedZtUrl
        }
    }

    
    // MARK: - Public Methods
    
    func received(notification:UNNotification) {
        print("Adding received notification to stack")
        self.notifications.append(notification)
        
        self.reloadTable()
    }
    
    func open(notification:UNNotification) {
        print("Navigating to opened notification")
        
        if !checkForExisting(notification:notification) {
            // Does not already exist in list
            self.received(notification: notification)
        }
        
        if let ztUrl = notification.request.content.userInfo["zturl"] as? String {
            self.selectedZtUrl = ztUrl
        }
        
        self.performSegue(withIdentifier: "showCouponDetails", sender: self)
    }
    
    
    // MARK: - Private Methods
    
    func reloadTable() {
        if self.notificationTableView != nil {
            self.notificationTableView.reloadData()
        }
    }
    
    func checkForExisting(notification: UNNotification) -> Bool {
        var exists = false
        
        for existingNotification in self.notifications {
            if existingNotification.request.identifier == notification.request.identifier {
                exists = true
                break
            }
        }
        
        return exists
    }
    
    
    // MARK: - UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.open(notification: self.notifications[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if self.notifications.count == 0 {
            
            return false
        }
        
        return true
    }
    
    
    // MARK: - UITableViewDataSource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.notifications.count == 0 {
            // Display NoResults cell
            return 1
        }
        
        return self.notifications.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.notifications.count == 0 {
            // Display NoResults cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "noResultsCell")!
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath) as! CouponTableViewCell
        
        cell.configureFor(notification: self.notifications[indexPath.row])
        
        return cell
    }
}

