//
//  CouponDetailsViewController.swift
//  zonetap_Example
//
//  Created by nlampi on 1/27/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class CouponDetailsViewController: UIViewController {
    
    @IBOutlet weak var webViewArea: UIView!
    var mainWebView: WKWebView = WKWebView()
    var ztUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainWebView.translatesAutoresizingMaskIntoConstraints = false
        self.webViewArea.addSubview(self.mainWebView)
        
        let views: [String:Any] = ["wV": mainWebView]
        var allConstraints: [NSLayoutConstraint] = []
        let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[wV]|", options: [], metrics: nil, views: views)
        allConstraints += hConstraints
        let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[wV]|", options: [], metrics: nil, views: views)
        allConstraints += vConstraints
        
        NSLayoutConstraint.activate(allConstraints)
        
        // Navigate to URL
        print("Navigating to URL: \(self.ztUrl)")
        let ztURL = URL(string: self.ztUrl)
        let ztRequest = URLRequest(url: ztURL!)
        self.mainWebView.load(ztRequest)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
