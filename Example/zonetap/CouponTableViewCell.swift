//
//  CouponTableViewCell.swift
//  zonetap_Example
//
//  Created by nlampi on 1/28/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class CouponTableViewCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        self.accessoryType = .disclosureIndicator
    }
    
    
    // MARK: - Public Methods
    
    func configureFor(notification:UNNotification) {
        self.titleLabel.text = notification.request.content.title
        self.detailsLabel.text = notification.request.content.body
    }
}
